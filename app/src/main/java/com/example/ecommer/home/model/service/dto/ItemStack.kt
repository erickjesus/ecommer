package com.example.ecommer.home.model.service.dto

data class ItemStack(

    val items: List<ItemX>,

    )