package com.example.ecommer.home.model.service.dto

data class Props(
    val pageProps: PageProps
)