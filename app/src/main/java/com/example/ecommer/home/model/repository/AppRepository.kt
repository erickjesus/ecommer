package com.example.ecommer.home.model.repository

import com.example.ecommer.home.model.database.dao.ItemSearchDao
import com.example.ecommer.home.model.database.entities.ItemSearch
import com.example.ecommer.home.model.service.dto.ResponseDTO
import com.example.ecommer.home.model.service.Service
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val service:Service,
    private val dao:ItemSearchDao
):AppRepositoryInterface {

    override suspend fun searchProduct(searchCriteria: String, numberPage: Long): ResponseDTO? {
        return service.searchProduct(searchCriteria, numberPage)
    }

    override suspend fun storSearch(item: ItemSearch) {
        dao.insertAll(item)
    }

    override suspend fun recoverSearch(): List<ItemSearch> {
      return dao.getAllItemSearch()
    }

}