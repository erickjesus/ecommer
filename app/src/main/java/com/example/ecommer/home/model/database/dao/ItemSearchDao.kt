package com.example.ecommer.home.model.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ecommer.home.model.database.entities.ItemSearch

@Dao
interface ItemSearchDao {

    @Query("SELECT * FROM item_search_table ORDER BY item_search DESC")
    suspend fun getAllItemSearch(): List<ItemSearch>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(itemSearch: ItemSearch)
}