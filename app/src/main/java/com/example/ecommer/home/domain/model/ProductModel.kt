package com.example.ecommer.home.domain.model

import com.example.ecommer.home.data.surce.remote.service.searchproduct.dto.ItemX

data class ProductModel(
    var nameProduct: String?,
    var priceProduct: Double?,
    var imageProduct: String?) {}

fun ItemX.toProductModel():ProductModel = ProductModel(name, price.toDouble(), image)