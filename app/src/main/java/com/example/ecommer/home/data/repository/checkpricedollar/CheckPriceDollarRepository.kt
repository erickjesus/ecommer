package com.example.ecommer.home.data.repository.checkpricedollar

import com.example.ecommer.home.data.surce.remote.service.checkdollarprice.ServiceCheckDollarPrice
import javax.inject.Inject

class CheckPriceDollarRepository @Inject constructor(
    private val service: ServiceCheckDollarPrice
) : CheckPriceDollarRepositoryInterface {

    override suspend fun getPriceDollar(): Double {
        return service.getPriceDollar()
    }

}