package com.example.ecommer.home.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ecommer.databinding.ActivityMainBinding
import com.example.ecommer.home.view.adapter.ProductAdapter
import com.example.ecommer.home.view.viewmodel.AppViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val model: AppViewModel by viewModels()
    private lateinit var adapter: ProductAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle = intent.extras
        if (bundle != null) {
            val search = bundle.getString("search").toString()
            binding.searchView.setQuery(search, false)
            binding.searchView.clearFocus()

            hideKeyBoard()

            model.setSearch(search)
            model.searchProduct()
        }

        initRecyclerView()

        binding.searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus ) {
                binding.searchView.setQuery("", false)

                val intent = Intent(this, FilterSearchActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.toString().isNotEmpty()) {
                    model.setSearch(query.toString().trim())
                    model.searchProduct()
                }
                binding.searchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        model.getPriceDollar()

        model.priceDollar.observe(this) {
            adapter.setPriceDollar(it)
        }

        model.isSearchisDifferent.observe(this) {
            if (it) {
                adapter.clear()
            }
        }

        model.isSuccess.observe(this) {
            if (it) {
                binding.recyclerView.visibility = View.VISIBLE
                binding.txtWithoutResults.visibility = View.GONE
            } else {
                binding.recyclerView.visibility = View.GONE
                binding.txtWithoutResults.visibility = View.VISIBLE
            }
        }

        model.listProducts.observe(this) {
            adapter.addAll(it)
        }

        model.showLoading.observe(this) {
            if (it) {
                binding.ContentLoading.visibility = View.VISIBLE
            } else {
                binding.ContentLoading.visibility = View.GONE
            }
        }

    }

    private fun initRecyclerView() {
        adapter = ProductAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        binding.recyclerView.adapter = adapter
    }


    private fun hideKeyBoard() {
        val view: View? = this.currentFocus
        if (view != null) {
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}
