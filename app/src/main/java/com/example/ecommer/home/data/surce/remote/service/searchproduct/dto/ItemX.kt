package com.example.ecommer.home.data.surce.remote.service.searchproduct.dto

data class ItemX(
    val image: String,
    val name: String,
    val price: Int
)