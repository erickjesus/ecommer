package com.example.ecommer.home.model.repository

import com.example.ecommer.home.model.database.entities.ItemSearch
import com.example.ecommer.home.model.service.dto.ResponseDTO


interface AppRepositoryInterface {
    suspend fun searchProduct(searchCriteria: String, numberPage: Long): ResponseDTO?

     suspend fun storSearch(item:ItemSearch)
     suspend fun recoverSearch():List<ItemSearch>
}