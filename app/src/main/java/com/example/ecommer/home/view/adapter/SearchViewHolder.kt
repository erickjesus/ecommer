package com.example.ecommer.home.view.adapter

import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.ecommer.databinding.ItemSearchLayoutBinding

class SearchViewHolder(private val binding: ItemSearchLayoutBinding):ViewHolder(binding.root) {

    fun render(search: String, onClickListener: (String) -> Unit){
        binding.textSearch.text = search
        itemView.setOnClickListener(){onClickListener(search)}
    }
}