package com.example.ecommer.home.data.surce.remote.service.api

import com.example.ecommer.home.data.surce.remote.service.checkdollarprice.dto.ResponseCheckDollarPriceDTO
import com.example.ecommer.home.data.surce.remote.service.searchproduct.dto.ResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Query

interface ApiSearchProduct {
    /*servicio para realizar busquedas de productos*/
    /*https://00672285.us-south.apigw.appdomain.cloud/demo-gapsi/search?&query=xbox&page=1*/
    @GET("demo-gapsi/search")
    suspend fun searchProduct(@HeaderMap headers:Map<String, String>, @Query( "query") searchCriteria:String, @Query("page") numberPage:Long):Response<ResponseDTO>

}