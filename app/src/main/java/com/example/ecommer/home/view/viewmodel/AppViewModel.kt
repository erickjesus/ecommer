package com.example.ecommer.home.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ecommer.home.domain.model.ProductModel
import com.example.ecommer.home.domain.usecase.SearchProductUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AppViewModel @Inject constructor(private val usecase: SearchProductUseCase) : ViewModel() {

    private val _listProducts = MutableLiveData<List<ProductModel>>()
    val listProducts:LiveData<List<ProductModel>> = _listProducts

    private val _showLoading = MutableLiveData<Boolean>()
    val showLoading:LiveData<Boolean> =_showLoading

    private val _isSuccess = MutableLiveData<Boolean>()
    val isSuccess:LiveData<Boolean> =_isSuccess

    private val _isSearchisDifferent= MutableLiveData<Boolean>()
    val isSearchisDifferent:LiveData<Boolean> = _isSearchisDifferent

    private val _priceDollar = MutableLiveData<Double>()
    val priceDollar:LiveData<Double> =_priceDollar

    fun setSearch(search:String){
        val isDifferent = usecase.setSearch(search)
        _isSearchisDifferent.postValue(isDifferent)
    }

    fun searchProduct() {
        _showLoading.postValue(true)
        viewModelScope.launch {
            val listProduct:List<ProductModel> = usecase.searchProduct()
            if (listProduct.isNotEmpty()) {
                _isSuccess.postValue(true)
            }else{
                _isSuccess.postValue(false)
            }
            _listProducts.postValue(listProduct)
            _showLoading.postValue(false)
        }
    }

    fun getPriceDollar(){
        viewModelScope.launch {
            _priceDollar.postValue(usecase.getPriceDollar())
        }
    }

//    fun saveSearch(str: String) {
//        viewModelScope.launch {
//            storeSearchUC.storSearch(str)
//        }
//    }

//    fun recoverSearch() {
//        viewModelScope.launch {
//            _listSearch.postValue(searchProductUC.recoverSearch())
//        }
//    }

}