package com.example.ecommer.home.data.surce.remote.service.checkdollarprice.dto

data class Info(
    val rate: Double,
    val timestamp: Int
)