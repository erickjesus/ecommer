package com.example.ecommer.home.model.service



import com.example.ecommer.home.model.service.api.Api
import com.example.ecommer.home.model.service.dto.ResponseDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class Service @Inject constructor(private val api: Api) {

    suspend fun searchProduct(searchCriteria: String, numberPage: Long): ResponseDTO? {
        return withContext(Dispatchers.IO) {
            val response:Response<ResponseDTO> = api.searchProduct(searchCriteria, numberPage)
            response.body()
        }

    }
}