package com.example.ecommer.home.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ecommer.home.domain.usecase.SearchProductUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FilterSearchViewModel @Inject constructor(private val usecase: SearchProductUseCase) :
    ViewModel() {

    private val _listSearch = MutableLiveData<List<String>>()
    val listSearch: LiveData<List<String>> = _listSearch

    fun getListSearch() {
        viewModelScope.launch {
            val list: List<String> = usecase.getListSearch()
            _listSearch.postValue(list)
        }
    }

}