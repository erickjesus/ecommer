package com.example.ecommer.home.view.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.ecommer.base.extension.formatMoney
import com.example.ecommer.base.extension.loadGlide
import com.example.ecommer.databinding.ItemProductLayoutBinding
import com.example.ecommer.home.domain.model.ProductModel

class ProductViewHolder(private val binding: ItemProductLayoutBinding) : ViewHolder(binding.root) {


    fun render(product: ProductModel) {
        binding.nameProduct.text = product.nameProduct
        val textPriceUSD = "$ ${product.priceProduct!!.formatMoney()} UDS"
        //val textPriceMXN = "$ ${(product.priceProduct!! * priceDollar).formatMoney()} MXN"
        binding.priceProduct.text = textPriceUSD
        // binding.priceProductMxn.text = textPriceMXN

        binding.imgProduct.loadGlide(product.imageProduct)
    }
}