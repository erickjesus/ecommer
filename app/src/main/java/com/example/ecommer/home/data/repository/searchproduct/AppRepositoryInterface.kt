package com.example.ecommer.home.data.repository.searchproduct

import com.example.ecommer.home.data.surce.local.database.entities.SearchEntity
import com.example.ecommer.home.domain.model.ProductModel


interface AppRepositoryInterface {
    suspend fun searchProduct(searchCriteria: String, numberPage: Long): List<ProductModel>
    suspend fun storeSearch(item: SearchEntity)
    suspend fun retrieveAllSearches(): List<String>
}