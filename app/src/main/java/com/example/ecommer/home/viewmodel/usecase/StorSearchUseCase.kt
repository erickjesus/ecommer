package com.example.ecommer.home.viewmodel.usecase

import com.example.ecommer.home.model.database.entities.ItemSearch
import com.example.ecommer.home.model.repository.AppRepository
import javax.inject.Inject

class StorSearchUseCase @Inject constructor(private val repository: AppRepository) {

    //private var list: MutableList<String> = mutableListOf()

     suspend fun storSearch(str:String){
        repository.storSearch(ItemSearch(0, str))

//        if(!list.contains(str)){
//            if(list.size == 4){
//                list.removeAt(0)
//            }
//            list.add(str)
//        }
        /*lalmado al probedor de datos*/

    }

     suspend fun recoverSearch(): MutableList<String> {
        val listItem =repository.recoverSearch()
        val list: MutableList<String> = mutableListOf()
        listItem.forEach { list.add(it.itemSearch) }
        return list
    }
}