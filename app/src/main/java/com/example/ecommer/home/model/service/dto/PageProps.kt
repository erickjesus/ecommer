package com.example.ecommer.home.model.service.dto

data class PageProps(val initialData: InitialData)