package com.example.ecommer.home.data.surce.remote.service.searchproduct.dto

data class ResponseDTO(
    val domainCode: String,
    val item: Item,
    val keyword: String,
    val responseMessage: String,
    val responseStatus: String,
    val sortStrategy: String
)