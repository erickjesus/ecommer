package com.example.ecommer.home.data.surce.remote.service.searchproduct.dto

data class InitialData(
    val searchResult: SearchResult
)