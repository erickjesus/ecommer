package com.example.ecommer.home.data.surce.remote.service

import com.example.ecommer.BuildConfig
import com.example.ecommer.home.data.surce.remote.service.api.ApiCheckDollarPrice
import com.example.ecommer.home.data.surce.remote.service.api.ApiSearchProduct
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ModueApi {

    @Singleton
    @Provides
    fun providerApi(): ApiSearchProduct {
        return buildApi(getHttpClient(),BuildConfig.URL_SEARCH_PRODUCT).create(ApiSearchProduct::class.java)
    }

    @Singleton
    @Provides
    fun providerApiPriceDollar(): ApiCheckDollarPrice {
        return buildApi(getHttpClient(),BuildConfig.URL_PRICE_DOLLAR).create(ApiCheckDollarPrice::class.java)
    }

    private fun getHttpClient():OkHttpClient{
        val httpClient = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)

        if(BuildConfig.DEBUG){
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)
        }
        return httpClient.build()
    }

    private fun buildApi(okHttpClient: OkHttpClient, urlBase:String):Retrofit{
        return Retrofit.Builder()
            .baseUrl(urlBase)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}