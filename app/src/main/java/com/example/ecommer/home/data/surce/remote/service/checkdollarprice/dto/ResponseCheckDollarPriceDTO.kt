package com.example.ecommer.home.data.surce.remote.service.checkdollarprice.dto

data class ResponseCheckDollarPriceDTO(
    val date: String,
    val info: Info,
    val query: Query,
    val result: Double,
    val success: Boolean
)