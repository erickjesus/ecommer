package com.example.ecommer.home.model.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "item_search_table")
data class ItemSearch(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id_item_search")val id:Long = 0,
    @ColumnInfo(name="item_search")val itemSearch:String
    )
