package com.example.ecommer.home.data.repository.checkpricedollar

interface CheckPriceDollarRepositoryInterface {
    suspend fun getPriceDollar(): Double
}