package com.example.ecommer.home.viewmodel.usecase

import com.example.ecommer.home.model.service.dto.ResponseDTO
import com.example.ecommer.home.model.repository.AppRepository
import javax.inject.Inject

class SearchProductUseCase @Inject constructor(private val repository: AppRepository) {

    suspend fun searchProduct(searchCriteria:String, numberPage:Long): ResponseDTO? = repository.searchProduct(searchCriteria, numberPage)
}