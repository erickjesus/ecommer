package com.example.ecommer.home.data.surce.remote.service.searchproduct.dto

data class SearchResult(val itemStacks: List<ItemStack>)