package com.example.ecommer.home.view.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.ecommer.databinding.ItemSearchLayoutBinding
import java.util.Locale


class SearchAdapter (private val onClickListener: (String)->Unit) : RecyclerView.Adapter<SearchViewHolder>(), Filterable {

    private var list: MutableList<String> = mutableListOf()
    private var listTemp: MutableList<String> = mutableListOf()
    private var filterList: MutableList<String> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSearchLayoutBinding.inflate(layoutInflater, parent, false)
        return SearchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val search = list[position]
        holder.render(search, onClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addAll(list: List<String>) {
        this.list.clear()
        this.list.addAll(list)

        listTemp.clear()
        listTemp.addAll(list)
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString().trim()

                filterList = if (charSearch.isEmpty()){
                    listTemp
                }else {
                    val resultList:MutableList<String> = mutableListOf()
                    for (row in listTemp) {
                        if (row.lowercase(Locale.ROOT).contains(charSearch.lowercase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = filterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                list = results?.values as MutableList<String>
                notifyDataSetChanged()
            }
        }
    }

}