package com.example.ecommer.home.data.surce.remote.service.checkdollarprice

import com.example.ecommer.BuildConfig
import com.example.ecommer.home.data.surce.remote.service.api.ApiCheckDollarPrice
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ServiceCheckDollarPrice @Inject constructor(private val api: ApiCheckDollarPrice) {

    suspend fun getPriceDollar():Double {
        return withContext(Dispatchers.IO) {
            val response = api.checkDollarPrice(getHeaders(), "MXN", "USD", 1)
            if(response.isSuccessful){
                response.body()!!.result
            }else{
                0.0
            }
        }
    }

    private fun getHeaders():HashMap<String, String>{
        val headers = HashMap<String, String>()
        headers["apikey"] = BuildConfig.PAI_KEY_PRICE_DOLLAR
        return headers
    }
}