package com.example.ecommer.home.model.service.dto

data class ItemX(
    val image: String,
    val name: String,
    val price: Int
)