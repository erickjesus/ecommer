package com.example.ecommer.home.data.surce.remote.service.searchproduct

import com.example.ecommer.BuildConfig
import com.example.ecommer.home.data.surce.remote.service.api.ApiSearchProduct
import com.example.ecommer.home.domain.model.ProductModel
import com.example.ecommer.home.domain.model.toProductModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class Service @Inject constructor(private val api: ApiSearchProduct) {

    suspend fun searchProduct(searchCriteria: String, numberPage: Long):List<ProductModel> {
        return withContext(Dispatchers.IO) {
            val response = api.searchProduct(getHeaders(), searchCriteria, numberPage)
            if(response.isSuccessful){
                response.body()!!.item.props.pageProps.initialData.searchResult.itemStacks[0].items.map { it.toProductModel() }
            }else{
                emptyList()
            }
        }
    }

    private fun getHeaders():HashMap<String, String>{
        val headers = HashMap<String, String>()
        headers["X-IBM-Client-Id"] = BuildConfig.KEY_PAI_SEARCH_PRODUCT
        return headers
    }
}