package com.example.ecommer.home.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.ecommer.home.model.database.dao.ItemSearchDao
import com.example.ecommer.home.model.database.entities.ItemSearch

@Database(entities = [ItemSearch::class], version = 1)
abstract class ItemSearchDataBase:RoomDatabase() {
    abstract fun getItemSearchDao():ItemSearchDao
}