package com.example.ecommer.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ecommer.home.model.service.dto.ItemX
import com.example.ecommer.home.viewmodel.usecase.SearchProductUseCase
import com.example.ecommer.home.viewmodel.usecase.StorSearchUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AppViewModel @Inject constructor(
    private val SearchProductUC: SearchProductUseCase,
    private val storSearchUC: StorSearchUseCase
) : ViewModel() {

    val listProducts = MutableLiveData<List<ItemX>?>()
    val showLoading = MutableLiveData<Boolean>()
    val listSearch = MutableLiveData<List<String>>()

    fun searchProduct(searchCriteria: String, numberPage: Long) {
        showLoading.postValue(true)
        viewModelScope.launch {
            val response = SearchProductUC.searchProduct(searchCriteria, numberPage)
            if (response != null) {
                val products: List<ItemX> =
                    response.item.props.pageProps.initialData.searchResult.itemStacks[0].items
                listProducts.postValue(products)
            } else {
                listProducts.postValue(emptyList())
            }
            showLoading.postValue(false)
        }
    }

    fun storSearch(str: String) {
        viewModelScope.launch {
            storSearchUC.storSearch(str)
        }
    }

    fun recoverSearch() {
        viewModelScope.launch {
            val list = storSearchUC.recoverSearch()
            listSearch.postValue(list)
        }
    }

}