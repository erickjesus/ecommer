package com.example.ecommer.home.data.surce.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ecommer.home.data.surce.local.database.entities.SearchEntity

@Dao
interface SearchDao {

    @Query("SELECT search FROM item_search_table ORDER BY date_creation ASC")
    suspend fun getAllSearch(): List<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(itemSearch: SearchEntity)
}