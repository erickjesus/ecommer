package com.example.ecommer.home.model.service.dto

data class InitialData(
    val searchResult: SearchResult
)