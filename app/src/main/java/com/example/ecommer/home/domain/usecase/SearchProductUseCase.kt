package com.example.ecommer.home.domain.usecase

import com.example.ecommer.home.data.surce.local.database.entities.SearchEntity
import com.example.ecommer.home.data.repository.checkpricedollar.CheckPriceDollarRepository
import com.example.ecommer.home.data.repository.searchproduct.AppRepository
import com.example.ecommer.home.domain.model.ProductModel
import java.util.Date
import javax.inject.Inject

class SearchProductUseCase @Inject constructor(
    private val rpSearchProduct: AppRepository,
    private val rpPriceDollar: CheckPriceDollarRepository

) {
    private var numberPage: Long = 1
    private var currentSearch = ""

    fun setSearch(search: String): Boolean {
        return if (search == currentSearch) {
            false
        } else {
            numberPage = 1
            currentSearch = search
            true
        }
    }

    suspend fun searchProduct(): List<ProductModel> {
        val r = rpSearchProduct.searchProduct(currentSearch, numberPage)
        if (r.isNotEmpty()) {
            rpSearchProduct.storeSearch(SearchEntity(currentSearch, Date().time))
        }
        numberPage++
        return r
    }

    suspend fun getListSearch():List<String>{
        return rpSearchProduct.retrieveAllSearches()
    }

    suspend fun getPriceDollar():Double{
        return rpPriceDollar.getPriceDollar()
    }
}