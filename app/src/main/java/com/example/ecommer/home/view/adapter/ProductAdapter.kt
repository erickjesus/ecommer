package com.example.ecommer.home.view.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ecommer.R
import com.example.ecommer.databinding.ItemProductLayoutBinding
import com.example.ecommer.home.domain.model.ProductModel

class ProductAdapter :RecyclerView.Adapter<ProductViewHolder>(){

    private var list:MutableList<ProductModel> = mutableListOf()
    private var priceDollar:Double = 0.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutInflater  = LayoutInflater.from(parent.context)
        val binding = ItemProductLayoutBinding.inflate(layoutInflater,parent, false)
        return  ProductViewHolder(binding)
        //val binding = (LayoutInflater.from(parent.context), parent, false)
        //return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val item = list[position]
        holder.render(item)
    }

    override fun getItemCount(): Int  = list.size




    fun addAll(list:List<ProductModel>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun clear(){
        list.clear()
        notifyDataSetChanged()
    }

    fun setPriceDollar(priceDollar: Double) {
        this.priceDollar = priceDollar
    }

//    fun starload(){
//        list.add(ProductModel("", 0.0,""))
//        notifyItemInserted(list.size - 1)
//    }

//    fun endLoad(){
//        list.removeAt(list.size - 1)
//        notifyItemRemoved(list.size)
//    }

    inner class ProductHolder(val binding: ItemProductLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}