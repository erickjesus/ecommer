package com.example.ecommer.home.data.surce.local.database

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RooModule {

    const val DATABASE_NAME = "itemsearch_data_base"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, SearchDataBase::class.java, DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideItemSearchDao(db: SearchDataBase) = db.getItemSearchDao()
}