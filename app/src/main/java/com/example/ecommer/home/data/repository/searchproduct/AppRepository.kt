package com.example.ecommer.home.data.repository.searchproduct

import com.example.ecommer.home.data.surce.local.database.dao.SearchDao
import com.example.ecommer.home.data.surce.local.database.entities.SearchEntity
import com.example.ecommer.home.data.surce.remote.service.searchproduct.Service
import com.example.ecommer.home.domain.model.ProductModel
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val service: Service,
    private val dao: SearchDao
): AppRepositoryInterface {

    override suspend fun searchProduct(searchCriteria: String, numberPage: Long):List<ProductModel>  {
        return service.searchProduct(searchCriteria, numberPage)
    }

    override suspend fun storeSearch(item: SearchEntity) {
        dao.insert(item)
    }

    override suspend fun retrieveAllSearches(): List<String> {
      return dao.getAllSearch()
    }

}