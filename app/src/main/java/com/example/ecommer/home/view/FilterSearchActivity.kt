package com.example.ecommer.home.view

import android.content.Intent
import android.os.Bundle
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ecommer.databinding.ActivityFilterSearchBinding
import com.example.ecommer.home.view.viewmodel.FilterSearchViewModel
import com.example.ecommer.home.view.adapter.SearchAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FilterSearchActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFilterSearchBinding

    private val model: FilterSearchViewModel by viewModels()
    private lateinit var adapter: SearchAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilterSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecyclerView()
        binding.filterSearch.requestFocus()

        model.getListSearch()

        model.listSearch.observe(this) {
            adapter.addAll(it)
        }

        binding.filterSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.toString().isNotEmpty()) {
                    starSearchProduct(query.toString().trim())
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return true
            }
        })
    }

    private fun initRecyclerView() {
        adapter = SearchAdapter() { search ->
            onItemSelect(search)
        }
        binding.listSearch.layoutManager = LinearLayoutManager(applicationContext)
        binding.listSearch.adapter = adapter
    }

    private fun onItemSelect(search: String) {
        binding.filterSearch.setQuery(search,false)
        binding.filterSearch.requestFocus()
    }

    private fun starSearchProduct(search:String){
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("search", search)
        startActivity(intent)
        finish()
    }
}