package com.example.ecommer.home.model.service.dto

data class ResponseDTO(
    val domainCode: String,
    val item: Item,
    val keyword: String,
    val responseMessage: String,
    val responseStatus: String,
    val sortStrategy: String
)