package com.example.ecommer.home.data.surce.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.ecommer.home.data.surce.local.database.dao.SearchDao
import com.example.ecommer.home.data.surce.local.database.entities.SearchEntity

@Database(entities = [SearchEntity::class], version = 1)
abstract class SearchDataBase:RoomDatabase() {
    abstract fun getItemSearchDao(): SearchDao
}