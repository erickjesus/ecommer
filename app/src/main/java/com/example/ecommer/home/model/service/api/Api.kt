package com.example.ecommer.home.model.service.api

import com.example.ecommer.home.model.service.dto.ResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface Api {
    @Headers("X-IBM-Client-Id:adb8204d-d574-4394-8c1a-53226a40876e")
    @GET("demo-gapsi/search")
    suspend fun searchProduct(@Query( "query") searchCriteria:String, @Query("page") numberPage:Long):Response<ResponseDTO>
}