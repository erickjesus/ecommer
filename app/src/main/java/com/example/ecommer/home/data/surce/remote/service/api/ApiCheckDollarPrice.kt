package com.example.ecommer.home.data.surce.remote.service.api

import com.example.ecommer.home.data.surce.remote.service.checkdollarprice.dto.ResponseCheckDollarPriceDTO
import com.example.ecommer.home.data.surce.remote.service.searchproduct.dto.ResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiCheckDollarPrice {

    /*servicio para consultar el precio del dolar al día*/
    /*https://api.apilayer.com/exchangerates_data/convert?to=MXN&from=USD&amount=1*/
    @GET("exchangerates_data/convert")
    suspend fun checkDollarPrice(@HeaderMap headers:Map<String, String>, @Query( "to") to:String, @Query("from") from:String, @Query("amount") amount:Int):Response<ResponseCheckDollarPriceDTO>
}