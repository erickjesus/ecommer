package com.example.ecommer.home.model.service.dto

data class SearchResult(val itemStacks: List<ItemStack>)