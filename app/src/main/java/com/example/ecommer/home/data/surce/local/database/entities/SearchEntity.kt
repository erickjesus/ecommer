package com.example.ecommer.home.data.surce.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "item_search_table")
data class SearchEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name="search")val search:String,
    @ColumnInfo(name="date_creation")val dateCreation:Long
    )
