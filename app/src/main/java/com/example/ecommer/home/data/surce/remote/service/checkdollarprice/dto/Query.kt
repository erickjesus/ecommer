package com.example.ecommer.home.data.surce.remote.service.checkdollarprice.dto

data class Query(
    val amount: Int,
    val from: String,
    val to: String
)