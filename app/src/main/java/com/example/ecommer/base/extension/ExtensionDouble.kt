package com.example.ecommer.base.extension

import java.text.DecimalFormat

//fun Double.format2Decimals():String{
//    return String.format("%.2f",this)
//}

fun Double.formatMoney():String{
    return String.format("%,.2f",this)
}