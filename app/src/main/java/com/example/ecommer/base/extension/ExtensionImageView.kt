package com.example.ecommer.base.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso

fun ImageView.loadPicasso(url: String?) {
    url.let { Picasso.get().load(url).into(this) }
}

fun ImageView.loadGlide(url: String?) {
    Glide.with(this.context).load(url).into(this)
}