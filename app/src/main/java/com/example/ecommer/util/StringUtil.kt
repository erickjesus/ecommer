package com.example.ecommer.util

class StringUtil {
    companion object{
        fun formatPreci(preci:Int):String{
            return "$ ${String.format("%1$,.2f", preci.toDouble())} USD"
        }

    }
}